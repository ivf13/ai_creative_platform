# AI CREATIVE PLATFORM

Проект мини-курса "Введение в промышленную разработку на Python".  
Хост: [ai-creative-platform.onrender.com](https://ai-creative-platform.onrender.com)  

Стек:
- Python 3.10
- Pipenv
- Uvicorn
- FastAPI
- SQLAlchemy
- Alembic
- Pydantic
- PostgreSQL
- Docker
- Jinja2
- Html
- Css
- JavaScript
- OpenAI